# v1.0.1

![fdc_logo_alixrocheleau](fdc_logo.png)

# framedatacreator

Open source C#/Razor/.NET/Electron desktop app (multiplatform build thanks to Electron) to generate flexible frame data (fighting games, platformers, beat'em up, etc.) exported as Json.

## Links

| Title    |
| ----------- | 
| [Chat](#chat)        | 
| [Setup](#setup-your-environement-for-this-project)        | 
| [Installs](#installs)        | 
| [How to use FrameDataCreator](#how-to-use-framedatacreator)        | 
| [Debug](#debug)        | 
| [Build](#build)        | 
| [License](#license)        | 
| [ChangeLog](CHANGELOG.md)        | 
| [Electron git](https://github.com/ElectronNET/Electron.NET)       | 
| [How to use Electron otherwise](#how-to-use-electron-otherwise)        | 
| [C# code convention](codeconvention.md)        | 

### Chat

[Discord Server](https://discord.gg/qyfaeTeGdH)

# Setup your environement for this project

Steps to use [Electron](https://github.com/ElectronNET/Electron.NET) with Visual Studio 2022

## Installs

[Install VS2022 community edition with standard options.](https://visualstudio.microsoft.com/downloads/) 
Note: the project currently uses .Net 6

[Install nodejs](https://nodejs.org/en/download/)

Install Electron (important to update the tool when updating the nugget package)

	dotnet tool install ElectronNET.CLI -g

    dotnet tool update ElectronNET.CLI -g

You can go ahead and clone the repository, open the solution file and execute the project in visual studio.

# How to use FrameDataCreator

[Wiki](../../wiki)

## Debug
Start your Electron.NET application. In Visual Studio attach to your running application instance. Go in the Debug Menu and click on Attach to Process.... Sort by your projectname and select it on the list.

## Build
Here you need the Electron.NET CLI as well. Type the following command in your ASP.NET Core folder:

    electronize build /target win

    electronize build /target win /PublishSingleFile false /PublishReadyToRun false

There are additional platforms available:

    electronize build /target win
    electronize build /target osx
    electronize build /target osx-arm64
    electronize build /target linux

Those four "default" targets will produce packages for those platforms.

Note that the osx-arm64 build requires that the project target net6.0. osx-arm64 is for Apple Silicon Macs.

For certain NuGet packages or certain scenarios you may want to build a pure x86 application. To support those things you can define the desired .NET Core runtime, the electron platform and electron architecture like this:

    electronize build /target custom "win7-x86;win32" /electron-arch ia32 

### License

[MIT license](LICENSE.md). For pre-compiled installer usage, please check the license for attribution if you use it in a project. 

### ChangeLog

Please view the [change log](changelog.md) file for all version changes.

# How to use Electron otherwise

1. Open VS2022 => New project => Blazor Server
2. Install the nuget package for ElectronNET.API -OR- through package manager `Install-Package ElectronNET.API`
3. Edit your Program.cs file then save your project and Build the solution

```
using ElectronNET.API;
var builder = WebApplication.CreateBuilder(args);
builder.Services.AddElectron();
builder.WebHost.UseElectron(args);
builder.Services.AddRazorPages();
builder.Services.AddServerSideBlazor();
//Add your services and controllers
var app = builder.Build();
// Configure the HTTP request pipeline.
if (!app.Environment.IsDevelopment())
{
    app.UseExceptionHandler("/Error");
    app.UseHsts();
}
app.UseHttpsRedirection();
app.UseStaticFiles();
app.UseRouting();
app.MapBlazorHub();
app.MapFallbackToPage("/_Host");
if (HybridSupport.IsElectronActive)
{
    CreateElectronWindow();
}
app.Run();
static async void CreateElectronWindow()
{
    //Very important to have this run in an async task otherwise Blazor won't start
    Task.Run(async () => { 
        BrowserWindow? window = await Electron.WindowManager.CreateWindowAsync();
        //Add global Shortcuts
        //https://www.electronjs.org/docs/latest/api/accelerator
        //Electron.GlobalShortcut.Register("CmdOrCtrl+B", ()=> { /*function here*/ } );

        window.OnClosed += () =>
        {
            //Electron.GlobalShortcut.UnregisterAll();        
            Electron.App.Quit();
            window = null;
        };
    });   
}
```

4. Go to the folder where your project's .csproj file is located
5. Open a command line and type `electronize init`
6. Then type to `electronize start` or `electronize start /watch`, this will also npm install before starting the app. If your build opens up a browser, you can freely close it as it is not needed. Note: if you didn't build first, Visual Studio will backup the csproj instead and electron will not launch properly, redo step 3.
7. The electron window is fully functionnal (and will update itself if you did /watch). You don't need to keep Visual Studio open if you would prefer to edit your code/files another way. Note that you can edit your Visual Studio config to start the app with the watch parameter as well so that you can hit Execute to launch electron and the browser version at the same time.

Note: if you are getting issues running the app from Visual studio, try updating the tool from cli and doing again electronize init and start.
Note 2: If the app hangs on the spash screen, try running it without the "/watch" parameter. You can run it with `electronize start` or remove the `/watch` param from the debugger launch args in visual studio.

## For full details and documentation about Electron, please see the [Electron git](https://github.com/ElectronNET/Electron.NET)

### Other Useful Hints

#### Fonts

You will probably want to download some fonts from Google [here](https://gwfh.mranftl.com/fonts) to include in your CSS since you want your app to function properly without being online.

#### Content Policy

You will probably want to add the following meta tag in your main layout/index.
```
<meta http-equiv="Content-Security-Policy" content="script-src 'self'; font-src 'self' data: blob:; img-src 'self' data: blob:; style-src 'self' 'unsafe-inline'">
```

This will give you warnings against using in-line Javascript. To use javascript properly, you should have dedicated javascript files in your root folders.
