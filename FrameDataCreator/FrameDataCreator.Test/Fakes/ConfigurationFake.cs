﻿using Microsoft.Extensions.Configuration;

namespace FrameDataCreator.Test.Fakes
{
    public static class ConfigurationFake
    {
        public static IConfiguration getConfig()
        {
            var config = new ConfigurationBuilder()
              .AddJsonFile("fakesettings.json")
              .Build();
            return config;
        }
    }
}
