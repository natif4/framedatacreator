﻿using FrameDataCreator.Constants;
using FrameDataCreator.Models.Common;

namespace FrameDataCreator.Models
{
    /// <summary>
    /// Area to display in the window layout
    /// </summary>
    public class Area : IIdentifier
    {
        /// <summary>
        /// Type of area to display
        /// </summary>
        public int Type { get; set; } = SD.BlankView;
        /// <summary>
        /// Width of the area
        /// </summary>
        public double Width { get; set; } = 0.0;
        /// <summary>
        /// Height of the area
        /// </summary>
        public double Height { get; set; } = 0.0;
        /// <summary>
        /// Which column is it part of
        /// </summary>
        public int Column { get; set; } = 0;
        /// <summary>
        /// Area name
        /// </summary>
        public string Name { get; set; } = "New Name";
        /// <summary>
        /// Guid
        /// </summary>
        public string Id { get; set; } = Guid.NewGuid().ToString();
    }
}
