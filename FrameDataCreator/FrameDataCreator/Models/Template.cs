﻿using FrameDataCreator.Models.Common;

namespace FrameDataCreator.Models
{
    /// <summary>
    /// Template to create layers from
    /// </summary>
    public class Template : IIdentifier
    {
        /// <summary>
        /// Constructor with a name
        /// </summary>
        /// <param name="name">Name of the new template</param>
        public Template(string name) { Name = name; }
        /// <summary>
        /// Basic constructor
        /// </summary>
        public Template() : this("New Template") { }
        /// <summary>
        /// Clone a template
        /// </summary>
        /// <returns>A deep copy of the template</returns>
        public Template Clone()
        {
            Template clone = new Template();
            clone.Name = Name;
            clone.GameId = GameId;
            foreach (var attr in Attributes)
            {
                clone.Attributes.Add(attr.Clone());
            }

            return clone;
        }
        /// <summary>
        /// Finds an attribute with a given type
        /// </summary>
        /// <typeparam name="T">The expected attribute type</typeparam>
        /// <param name="type">The type to search for as integer</param>
        /// <returns></returns>
        public T? FindAttribute<T>(int type = 0)
        {
            return (T?)(Attributes?.FirstOrDefault(a => a.PropertyType == type));
        }
        /// <summary>
        /// Guid
        /// </summary>
        public string Id { get; set; } = Guid.NewGuid().ToString();
        /// <summary>
        /// Game Id, related to something meaningful in your game projects
        /// </summary>
        public virtual string GameId { get; set; }
        /// <summary>
        /// Name
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// Attributes contained in this template
        /// </summary>
        public List<IAttribute> Attributes { get; set; } = new List<IAttribute>();
    }
}
