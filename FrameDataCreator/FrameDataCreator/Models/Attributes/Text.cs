﻿using FrameDataCreator.Constants;
using FrameDataCreator.Models.Common;

namespace FrameDataCreator.Models.Attributes
{
    /// <summary>
    /// Text attribute
    /// </summary>
    public class Text : IAttribute
    {
        private string _value = "";
        private string _default="";    
        /// <summary>
        /// Default value
        /// </summary>
        public object Default { get => _default; set => _default = (String)value; }
        /// <summary>
        /// Curret value
        /// </summary>
        public object Value { get => _value; set => _value = (String)value; }
        /// <summary>
        /// Type of the attribute
        /// </summary>
        public int PropertyType { get; } = SD.Text;
        /// <summary>
        /// Name of attribute
        /// </summary>
        public string Name { get; set; } = "New Text";
        /// <summary>
        /// Guid
        /// </summary>
        public string Id { get; set; } = Guid.NewGuid().ToString();
        /// <summary>
        /// Clone an attribute as deep copy
        /// </summary>
        /// <returns>A deep copy of the attribute</returns>
        public IAttribute Clone()
        {
            var clone = new Text();
            clone.Default = Default;
            clone.Id = Id;
            clone.Name = Name;
            clone.Value = Value;
            return clone;
        }
    }
}
