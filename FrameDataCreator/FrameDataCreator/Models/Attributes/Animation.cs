﻿using FrameDataCreator.Constants;
using FrameDataCreator.Models.Common;

namespace FrameDataCreator.Models.Attributes
{
    /// <summary>
    /// Animation attribute for templates and layers
    /// </summary>
    public class Animation : IAttribute
    {
        private string _default = "folderpath";
        private string _value = "";
        /// <summary>
        /// Constructor
        /// </summary>
        public Animation()
        {
            Name = "New Animation";
        }
        /// <summary>
        /// This is used to display the images in the animation
        /// </summary>
        public List<string> ImageListPath { get; set; } = new List<string>();
        /// <summary>
        /// Used to create new instances of image layer from image files in the folder picked
        /// </summary>
        public string ImageTemplateToCreate { get; set; } = "gameId";
        /// <summary>
        /// Layer Ids of the image instances created 
        /// </summary>
        public List<string> ImagesLayersUID { get; set; } = new List<string>();
        /// <summary>
        /// Default value
        /// </summary>
        public object Default { get => _default; set => _default = (String)value; }
        /// <summary>
        /// Current value
        /// </summary>
        public object Value { get => _value; set => _value = (String)value; }
        /// <summary>
        /// Type of this attribute
        /// </summary>
        public int PropertyType { get; } = SD.Animation;
        /// <summary>
        /// Name of attribute
        /// </summary>
        public string Name { get; set; } = "New animation";
        /// <summary>
        /// GUID 
        /// </summary>
        public string Id { get; set; } = Guid.NewGuid().ToString();
        /// <summary>
        /// Clone an attribute as deep copy
        /// </summary>
        /// <returns>A deep copy of the attribute</returns>
        public IAttribute Clone()
        {
            var clone = new Animation();
            clone.Default = Default;
            clone.Id = Id;
            clone.Name = Name;
            clone.Value = Value;
            clone.ImageTemplateToCreate = ImageTemplateToCreate;
            foreach (var imagePath in ImageListPath)
            {
                clone.ImageListPath.Add(imagePath);
            }
            foreach (var imageUid in ImagesLayersUID)
            {
                clone.ImagesLayersUID.Add(imageUid);
            }
            return clone;
        }
    }
}
