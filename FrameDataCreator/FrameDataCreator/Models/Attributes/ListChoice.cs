﻿using FrameDataCreator.Constants;
using FrameDataCreator.Models.Common;

namespace FrameDataCreator.Models.Attributes
{
    /// <summary>
    /// A select list of choices
    /// </summary>
    public class ListChoice :  IAttribute
    {
        private string _default ="";
        private string _value ="";
        /// <summary>
        /// List of choices 
        /// </summary>
        /// <see cref="Choice"/>
        public List<Choice> Choices { get; set; } = new List<Choice>();
        /// <summary>
        /// Default value
        /// </summary>
        public object Default { get => _default; set => _default = (String)value; }
        /// <summary>
        /// Curret value
        /// </summary>
        public object Value { get => _value; set => _value = (String)value; }
        /// <summary>
        /// Type of the attribute
        /// </summary>
        public int PropertyType { get; } = SD.ListChoice;
        /// <summary>
        /// Name of attribute
        /// </summary>
        public string Name { get; set; } = "New List Choice";
        /// <summary>
        /// Guid
        /// </summary>
        public string Id { get; set; } = Guid.NewGuid().ToString();
        /// <summary>
        /// Clone an attribute as deep copy
        /// </summary>
        /// <returns>A deep copy of the attribute</returns>
        public IAttribute Clone()
        {
            var clone = new ListChoice();
            clone.Default = Default;
            clone.Value = Value;
            clone.Id = Id;
            clone.Name = Name;
            foreach (var choice in Choices)
            {
                clone.Choices.Add(
                    new Choice()
                    {
                        NameValuePair = new KeyValuePair<string, string>(choice.NameValuePair.Key, choice.NameValuePair.Value)
                    }
                );
            }
            return clone;
        }
    }
}
