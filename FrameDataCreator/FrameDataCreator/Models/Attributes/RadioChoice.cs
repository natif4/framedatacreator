﻿using FrameDataCreator.Constants;
using FrameDataCreator.Models.Common;
using Newtonsoft.Json.Linq;

namespace FrameDataCreator.Models.Attributes
{
    /// <summary>
    /// A list of choices of radio buttons, only 1 selectable
    /// </summary>
    public class RadioChoice : IAttribute
    {
        private string _default = "";
        private string _value = "";
        /// <summary>
        /// Default value
        /// </summary>
        public object Default { get => _default; set => _default = (String)value; }
        /// <summary>
        /// Curret value
        /// </summary>
        public object Value { get => _value; set => _value = (String)value; }
        public List<Choice> Choices { get; set; } = new List<Choice>();
        /// <summary>
        /// Type of the attribute
        /// </summary>
        public int PropertyType { get; } = SD.RadioChoice;
        /// <summary>
        /// Name of attribute
        /// </summary>
        public string Name { get; set; } = "New Radio";
        /// <summary>
        /// Guid
        /// </summary>
        public string Id { get; set; } = Guid.NewGuid().ToString();
        /// <summary>
        /// Clone an attribute as deep copy
        /// </summary>
        /// <returns>A deep copy of the attribute</returns>
        public IAttribute Clone()
        {
            var clone = new RadioChoice();
            clone.Default = Default;
            clone.Value = Value;
            clone.Id = Id;
            clone.Name = Name;
            foreach (var choice in Choices)
            {
                clone.Choices.Add(
                    new Choice()
                    {
                        NameValuePair = new KeyValuePair<string, string>(choice.NameValuePair.Key, choice.NameValuePair.Value)
                    }
                );
            }
            return clone;
        }
    }
}
