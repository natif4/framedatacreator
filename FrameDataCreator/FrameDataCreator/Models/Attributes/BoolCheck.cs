﻿using FrameDataCreator.Constants;
using FrameDataCreator.Models.Common;

namespace FrameDataCreator.Models.Attributes
{
    /// <summary>
    /// Boolean check mark attribute
    /// </summary>
    public class BoolCheck :  IAttribute
    {
        private bool _default=false;
        private bool _value=false;
        /// <summary>
        /// Default value
        /// </summary>
        public object Default { get => _default; set => _default = Convert.ToBoolean(value); }
        /// <summary>
        /// Current value
        /// </summary>
        public object Value { get => _value; set => _value = Convert.ToBoolean(value); }
        /// <summary>
        /// Text label for the checkmark
        /// </summary>
        public string Text { get; set; } = "Label";
        /// <summary>
        /// Type of attribute
        /// </summary>
        public int PropertyType { get; } = SD.BoolCheck;
        /// <summary>
        /// Name of the attribute
        /// </summary>
        public string Name { get; set; } = "New BoolCheck";
        /// <summary>
        /// Guid
        /// </summary>
        public string Id { get; set; } = Guid.NewGuid().ToString();
        /// <summary>
        /// Clone an attribute as deep copy
        /// </summary>
        /// <returns>A deep copy of the attribute</returns>
        public IAttribute Clone()
        {
            var clone = new BoolCheck();
            clone.Default = Default;
            clone.Id = Id;
            clone.Name = Name;
            clone.Value = Value;
            clone.Text = Text;
            return clone;
        }
    }
}
