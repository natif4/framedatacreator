﻿using FrameDataCreator.Constants;
using FrameDataCreator.Models.Common;

namespace FrameDataCreator.Models.Attributes
{
    /// <summary>
    /// Hit box attribute
    /// </summary>
    public class HitBox : IAttribute
    {
        private int _default = 0;
        private int _value = 0;
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="position">Position</param>
        /// <param name="hBColor">Color</param>
        public HitBox(Position position, HBColor? hBColor = null)
        {
            Position = position;
            Color = hBColor ?? new HBColor();
            Name = "Box";
        }
        /// <summary>
        /// Color of the hitbox
        /// </summary>
        /// <see cref="HBColor"/>
        public HBColor Color { get; set; }
        /// <summary>
        /// Position of the box
        /// </summary>
        /// <see cref="Position"/>
        public Position Position { get; set; }
        /// <summary>
        /// Width in pixels
        /// </summary>
        public int Width { get; set; } = 32;
        /// <summary>
        /// Height in pixels
        /// </summary>
        public int Height { get; set; } = 32;
        /// <summary>
        /// Default value
        /// </summary>
        public object Default { get => _default; set => _default = Convert.ToInt32(value); }
        /// <summary>
        /// Curret value
        /// </summary>
        public object Value { get => _value; set => _value = Convert.ToInt32(value); }
        /// <summary>
        /// Type of the attribute
        /// </summary>
        public int PropertyType { get; } = SD.HitBox;
        /// <summary>
        /// Name of attribute
        /// </summary>
        public string Name { get; set; } = "New HitBox";
        /// <summary>
        /// Guid
        /// </summary>
        public string Id { get; set; } = Guid.NewGuid().ToString();
        /// <summary>
        /// Clone an attribute as deep copy
        /// </summary>
        /// <returns>A deep copy of the attribute</returns>
        public IAttribute Clone()
        {
            var clone = new HitBox(
                new Position(Position.X, Position.Y),
                new HBColor() { Blue = Color.Blue, Green = Color.Green, Red = Color.Red }
                );
            clone.Default = Default;
            clone.Value = Value;
            clone.Height = Height;
            clone.Width = Width;
            clone.Name = Name;
            return clone;
        }
    }
}
