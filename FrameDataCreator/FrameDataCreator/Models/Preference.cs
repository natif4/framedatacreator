﻿namespace FrameDataCreator.Models
{
    /// <summary>
    /// A preference setting
    /// </summary>
    public class Preference
    {
        /// <summary>
        /// Name
        /// </summary>
        public string Name { get; set; } = "New Preference";
        /// <summary>
        /// value as a string
        /// </summary>
        public string Value { get; set; } = "";
        /// <summary>
        /// Description of the preference setting
        /// </summary>
        public string Description { get; set; } = "";
        /// <summary>
        /// Type of input to show/edit
        /// </summary>
        public int Type { get; set; } = 0;
    }
}
