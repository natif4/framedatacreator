﻿namespace FrameDataCreator.Models
{
    /// <summary>
    /// FrameDataCreator Project
    /// </summary>
    public class Project
    {
        /// <summary>
        /// Name
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// Working directory subfolder to save projects
        /// </summary>
        public string Path { get; set; } = "projects";
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="name">Name of the new project</param>

        public Project(string name = "New project") { Name = name; }
        /// <summary>
        /// Layouts (tab) available
        /// </summary>
        public List<Layout> Layouts { get; set; } = new List<Layout>();
        /// <summary>
        /// Template list to create layers from
        /// </summary>
        public List<Template> Templates { get; set; } = new List<Template>();
        /// <summary>
        /// Layers of data in the project
        /// </summary>
        public List<Layer> Layers { get; set; } = new List<Layer>();

        public List<Layer> GetFilteredLayers(string layerName)
        {
            var result = new List<Layer>();
            foreach (var layer in Layers)
            {
                result.AddRange(layer.GetFilteredLayers(layerName));
            }
            return result;
        }
    }
}
