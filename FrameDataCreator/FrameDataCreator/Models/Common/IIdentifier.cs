﻿namespace FrameDataCreator.Models.Common
{
    /// <summary>
    /// Interface for Ids
    /// </summary>
    public interface IIdentifier
    {
        /// <summary>
        /// Guid id used internally
        /// </summary>
        string Id { get; set; }
        /// <summary>
        /// Name of the object
        /// </summary>
        string Name { get; set; }
    }
}