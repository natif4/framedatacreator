﻿namespace FrameDataCreator.Models.Common
{
    /// <summary>
    /// A color component
    /// </summary>
    public class HBColor
    {
        /// <summary>
        /// Constructor with color params
        /// </summary>
        /// <param name="red">Red amount</param>
        /// <param name="green">Green amount</param>
        /// <param name="blue">Blue amount</param>
        public HBColor(int red=255, int green = 255, int blue = 255) {
            Red= red;
            Green= green;
            Blue= blue;
        }
        /// <summary>
        /// Red amount
        /// </summary>
        public int Red { get; set; }
        /// <summary>
        /// Green amount
        /// </summary>
        public int Green { get; set; }
        /// <summary>
        /// Blue amount
        /// </summary>
        public int Blue { get; set; }   
    }
}
