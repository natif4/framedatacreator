﻿namespace FrameDataCreator.Models.Common
{
    /// <summary>
    /// Position represented by X and Y in pixels
    /// </summary>
    public class Position
    {
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="x">X position</param>
        /// <param name="y">Y position</param>
        public Position(int x = 0, int y = 0)
        {
            X = x;
            Y = y;
        }
        /// <summary>
        /// X value in pixels
        /// </summary>
        public int X { get; set; }
        /// <summary>
        /// Y value in pixels
        /// </summary>
        public int Y { get; set; }

    }
}
