﻿using FrameDataCreator.Models.Common;

namespace FrameDataCreator.Models
{
    /// <summary>
    /// Layout display for a tab
    /// </summary>
    public class Layout : IIdentifier
    {
        /// <summary>
        /// List of areas contained in the layout
        /// </summary>
        public List<Area> Areas { get; set; } = new List<Area>();
        /// <summary>
        /// Name
        /// </summary>
        public string Name { get; set; } = "New Name";
        //Guid
        public string Id { get; set; } = Guid.NewGuid().ToString();
    }
}
