﻿namespace FrameDataCreator.Models
{
    /// <summary>
    /// Settings for FrameDataCreator
    /// </summary>
    public class Settings
    {
        /// <summary>
        /// List of project prefabs
        /// </summary>
        public List<ProjectPrefab> ProjectPrefabs { get; set; } = new List<ProjectPrefab>();
        /// <summary>
        /// List of preferences
        /// </summary>
        public List<Preference> Preferences { get; set; } = new List<Preference>();
    }
}
