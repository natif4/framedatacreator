using ElectronNET.API;
using ElectronNET.API.Entities;
using FrameDataCreator.Controllers;
using FrameDataCreator.Data;
using FrameDataCreator.Service;

var builder = WebApplication.CreateBuilder(args);
builder.Services.AddElectron();
builder.WebHost.UseElectron(args);
// Add services to the container.
builder.Services.AddRazorPages();
builder.Services.AddServerSideBlazor();
builder.Services.AddScoped<IRefreshService, RefreshService>();
builder.Services.AddSingleton<FDCState>();
builder.Services.AddSingleton<ApplicationController>();
var app = builder.Build();
// Configure the HTTP request pipeline.
if (!app.Environment.IsDevelopment())
{
    app.UseExceptionHandler("/Error");
    app.UseHsts();
}
app.UseHttpsRedirection();
app.UseStaticFiles();
app.UseRouting();
app.MapBlazorHub();
app.MapFallbackToPage("/_Host");
if (HybridSupport.IsElectronActive)
{
    CreateElectronWindow();
}
LoadAppState();

app.Run();

void LoadAppState()
{
    using (var scope = app.Services.CreateScope())
    {
        var appState = scope.ServiceProvider.GetRequiredService<FDCState>();
        appState.Start("fdcsettings.json");
    }
}
static async void CreateElectronWindow()
{    
    //Very important to have this run in an async task otherwise Blazor won't start
    await Task.Run(async () => {        
        BrowserWindow? window = await Electron.WindowManager.CreateWindowAsync(
            new BrowserWindowOptions() { 
                Title="FrameDataCreator", 
                Width=1920, 
                Height=1080,
                WebPreferences = new WebPreferences()
                {
                    WebSecurity = false,
                    AllowRunningInsecureContent = false,
                },
               Show=false,                
            });
        window.RemoveMenu();
        window.Maximize();
        window.Show();
        window.WebContents.OpenDevTools(); // <- remove this before compile/release
        //Add global Shortcuts
        //https://www.electronjs.org/docs/latest/api/accelerator
        //Electron.GlobalShortcut.Register("CmdOrCtrl+D", ()=> { /*function here*/ } );

        window.OnClosed += () =>
        {
            //Electron.GlobalShortcut.UnregisterAll();        
           // Electron.App.Quit(); //<- this might be causing issues, leaving node open and webview open but the app is "closed"
            window = null;
        };
    });   
}