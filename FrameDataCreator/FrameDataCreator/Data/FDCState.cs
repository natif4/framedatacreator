﻿using FrameDataCreator.Constants;
using FrameDataCreator.Models;
using Newtonsoft.Json;
using System.Security.Cryptography.Xml;

namespace FrameDataCreator.Data
{
    /// <summary>
    /// Application State that contains all data
    /// </summary>
    public class FDCState
    {
        private readonly IConfiguration _configuration;
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="configuration">DI config</param>
        public FDCState(IConfiguration configuration)
        {

            _configuration = configuration;
        }
        /// <summary>
        /// Start the app and load settings
        /// </summary>
        /// <param name="fdcSettings">json path for the fdcsettings</param>
        public void Start(string fdcSettings)
        {

            Version = _configuration["Version"];
            GenerateStartSettings();
            var filePath = Path.Combine(Directory.GetCurrentDirectory(), fdcSettings);
            var mustSave = false;
            if (File.Exists(filePath))
            {
                FdcSettings fileSettings = JsonConvert.DeserializeObject<FdcSettings>(File.ReadAllText(filePath));
                //check prefabs
                var loadedPrefabs = fileSettings.Settings.ProjectPrefabs;

                foreach (var prefab in loadedPrefabs)
                {
                    if (!Settings.ProjectPrefabs.Any(p => p.Name == prefab.Name))
                    {
                        Settings.ProjectPrefabs.Add(prefab);
                        mustSave = true;
                    }
                    else
                    {

                        var existingPrefab = Settings.ProjectPrefabs.FirstOrDefault(p => p.Name == prefab.Name);
                        if (prefab != null)
                        {
                            existingPrefab.Name = prefab.Name;
                            existingPrefab.Json = prefab.Json;
                        }
                    }
                }

                //check preferences
                var loadedSettings = fileSettings.Settings.Preferences;
                foreach (var preference in loadedSettings)
                {
                    var existingPref = Settings.Preferences.FirstOrDefault(p => p.Name == preference.Name);
                    if (existingPref != null)
                    {
                        existingPref.Name = preference.Name;
                        existingPref.Value = preference.Value;
                    }
                }
            }
            else mustSave = true;
            if (mustSave) SaveSettings(fdcSettings);
            LoadPrefabs();
            GenerateStartProject();

        }
        /// <summary>
        /// Creates a new project
        /// </summary>
        /// <param name="name">Name of the project</param>
        /// <param name="prefabName">Prefab to create from</param>
        /// <param name="blank">is it a blank project</param>
        public void NewProject(string name, string prefabName = "Base", bool blank = true)
        {
            if (blank)
            {
                GenerateStartProject();
            }
            else
            {
                Project = OpenProject(ProjectFromPrefab(prefabName));
                Project.Name = name;
            }
        }
        /// <summary>
        /// Opens a project
        /// </summary>
        /// <param name="project">Project to open</param>
        /// <returns>returns the project reference</returns>
        public Project OpenProject(Project project)
        {
            Project = project;
            foreach (var layer in Project.Layers)
            {
                RestoreParentReferenceFromId(layer);
            }
            return Project;
        }
        /// <summary>
        /// Load project prefabs list
        /// </summary>
        public void LoadPrefabs()
        {
            Prefabs = new List<string>();
            foreach (var prefab in Settings.ProjectPrefabs)
            {
                Prefabs.Add(prefab.Name);
            }
        }
        /// <summary>
        /// Creates a project from a prefab
        /// </summary>
        /// <param name="name">The prefab name to clone</param>
        /// <returns>A new project</returns>
        public Project ProjectFromPrefab(string name)
        {
            var prefab = Settings.ProjectPrefabs.First(p => p.Name.ToLower() == name.ToLower());
            var project = JsonConvert.DeserializeObject<Project>(prefab.Json, new JsonSerializerSettings
            {
                TypeNameHandling = TypeNameHandling.Auto
            });
            return project;
        }
        /// <summary>
        /// Deletes a project prefab from the list
        /// </summary>
        /// <param name="name">The name of the prefab to delete</param>
        public void DeletePrefab(string name)
        {
            Settings.ProjectPrefabs.RemoveAll(p => p.Name.ToLower() == name.ToLower());
            SaveSettings();
            LoadPrefabs();
        }
        /// <summary>
        /// Renames a project prefab with a new name
        /// </summary>
        /// <param name="newName">The new name</param>
        /// <param name="oldName">The old name to change</param>
        public void RenamePrefab(string newName, string oldName)
        {
            if (newName != null && oldName != null)
            {
                var prefab = Settings.ProjectPrefabs.First(p => p.Name.ToLower() == oldName.ToLower());
                prefab.Name = newName;
                SaveSettings();
                LoadPrefabs();
            }
            else
            {
                //Return a message error for statusBar
            }
        }
        /// <summary>
        /// Recursively restores the parent object reference for each children layers
        /// </summary>
        /// <param name="layer"></param>
        private void RestoreParentReferenceFromId(Layer layer)
        {
            foreach (var layerChild in layer.Children)
            {
                layerChild.Parent = layer;
                RestoreParentReferenceFromId(layerChild);
            }
        }
        /// <summary>
        /// Saves current settings
        /// </summary>
        /// <returns>True if successful</returns>
        public bool SaveSettings(string fdcsettings = "fdcsettings.json")
        {
            try
            {
                var filePath = Path.Combine(Directory.GetCurrentDirectory(), fdcsettings);
                var jsonString = JsonConvert.SerializeObject(new FdcSettings() { Version = _configuration["Version"], Settings = Settings }, Formatting.Indented);
                File.WriteAllText(filePath, jsonString);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
                return false;
            }
            return true;
        }
        /// <summary>
        /// Saves current project 
        /// </summary>
        /// <returns>True if successful</returns>
        public bool SaveProject()
        {
            try
            {
                var filePath = Path.Combine(Path.Combine(Directory.GetCurrentDirectory(), Project.Path), $"{Project.Name}.fdc");
                var jsonString = JsonConvert.SerializeObject(Project, Formatting.Indented, new JsonSerializerSettings
                {
                    TypeNameHandling = TypeNameHandling.All
                });
                File.WriteAllText(filePath, jsonString);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
                return false;
            }
            return true;
        }
        /// <summary>
        /// Saves the current project as a prefab
        /// </summary>
        /// <returns>True if successful</returns>
        public bool SaveProjectAsPrefab()
        {
            try
            {
                var jsonString = JsonConvert.SerializeObject(Project, Formatting.Indented, new JsonSerializerSettings
                {
                    TypeNameHandling = TypeNameHandling.All
                });

                Settings.ProjectPrefabs.Add(new ProjectPrefab() { Name = Project.Name, Json = jsonString });
                SaveSettings();
                LoadPrefabs();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
                return false;
            }
            return true;
        }
        /// <summary>
        /// Generates application start settings
        /// </summary>
        private void GenerateStartSettings()
        {
            Settings = new Settings();
            Settings.ProjectPrefabs.Add(
                new ProjectPrefab()
                {
                    Name = "Blank",
                    Json = _configuration["ProjectPrefabs:Blank"],
                }
                );
            Settings.ProjectPrefabs.Add(
               new ProjectPrefab()
               {
                   Name = "Base",
                   Json = _configuration["ProjectPrefabs:Base"],
               }
                ); ;
            GenerateDefaultSettings();
        }
        /// <summary>
        /// Generates the default settings
        /// </summary>
        private void GenerateDefaultSettings()
        {
            Settings.Preferences.Add(new Preference()
            {
                Name = "Layers Export",
                Value = "true",
                Type = SD.PrefBool,
                Description = "Layers will be converted to struct properties instead of being inside a Children list property. (Recommended On)"
            });
            Settings.Preferences.Add(new Preference()
            {
                Name = "Attributes Export",
                Value = "true",
                Type = SD.PrefBool,
                Description = "Attributes will be converted to struct properties instead of being inside an Attributes list property. Must have Layers Export On. (Recommended On)"
            });
            Settings.Preferences.Add(new Preference()
            {
                Name = "Hitbox Export",
                Value = "true",
                Type = SD.PrefBool,
                Description = "Hitboxes attributes will be grouped as a list property, grouped by value, instead of being individual attributes. Must have Attributes Export On. (Recommended On)"
            });
            Settings.Preferences.Add(new Preference()
            {
                Name = "Animation Speed",
                Value = "500",
                Type = SD.PrefInt,
                Description = "Time in ms for the animation speed in the animation view. This is not related to any attributes or game related, it is only used here."
            });
            Settings.Preferences.Add(new Preference()
            {
                Name = "Animation AutoChild",
                Value = "true",
                Type = SD.PrefBool,
                Description = "Automatically create an empty Layer to contain the image layers created from the animation confirmation. (Recommended On)"
            });
            Settings.Preferences.Add(new Preference()
            {
                Name = "Project Prefabs",
                Value = "true",
                Type = SD.PrefPrefab,
                Description = ""
            });
        }
        /// <summary>
        /// Generates default project
        /// </summary>
        private void GenerateStartProject()
        {
            Project = new Project();
            Project.Layouts = GenerateStartLayouts();
        }
        /// <summary>
        /// Generates the default start layouts
        /// </summary>
        /// <returns>List of window area layouts</returns>
        private List<Layout> GenerateStartLayouts()
        {
            return new List<Layout>() {
                    new Layout() {
                        Name= "layout",
                        Areas= new List<Area>()
                        {
                            new Area() {
                                Name="ImageView",
                                Column= 1,
                                Type=SD.ImageView,
                                Width= 75.0,
                                Height= 72.0,
                            },
                            new Area() {
                                Name="SpriteView",
                                Column= 1,
                                Type=SD.SpriteView,
                                Width= 00.0,
                                Height= 00.0,
                            },
                            new Area() {
                                Name="TreeView",
                                Column= 2,
                                Type=SD.TreeView,
                                Width= 00.0,
                                Height= 25.0,
                            },
                            new Area() {
                                Name="Properties",
                                Column= 2,
                                Type=SD.PropertiesView,
                                Width= 00.0,
                                Height= 00.0,
                            },
                        },
                    },
                    new Layout(){
                        Name="animation",
                        Areas= new List<Area>()
                        {
                            new Area() {
                                Name="ImageView",
                                Column= 1,
                                Type=SD.ImageView,
                                Width= 75.0,
                                Height= 72.0,
                            },
                            new Area() {
                                Name="SpriteView",
                                Column= 1,
                                Type=SD.SpriteView,
                                Width= 00.0,
                                Height= 00.0,
                            },
                            new Area() {
                                Name="AnimationView",
                                Column= 2,
                                Type=SD.AnimationView,
                                Width= 00.0,
                                Height= 52.0,
                            },
                            new Area() {
                                Name="TreeView",
                                Column= 2,
                                Type=SD.TreeView,
                                Width= 00.0,
                                Height= 00.0,
                            },
                            new Area() {
                                Name="Properties",
                                Column= 2,
                                Type=SD.PropertiesView,
                                Width= 00.0,
                                Height= 00.0,
                            },
                        },
                    },
                    new Layout(){
                        Name="templateedit",
                        Areas= new List<Area>()
                        {
                            new Area() {
                                Name="TemplateTreeView",
                                Column= 1,
                                Type=SD.TemplateTreeView,
                                Width= 25.0,
                                Height= 100.0,
                            },
                            new Area() {
                                Name="Properties",
                                Column= 2,
                                Type=SD.TemplatePropertiesView,
                                Width= 00.0,
                                Height= 100.0,
                            }
                        }
                    },
            };
        }
        /// <summary>
        /// Current application version
        /// </summary>
        public string? Version { get; set; }
        /// <summary>
        /// Loaded project
        /// </summary>
        public Project Project { get; set; } = new Project();
        /// <summary>
        /// Project prefabs
        /// </summary>
        public List<string> Prefabs { get; set; } = new List<string>();
        /// <summary>
        /// Settings object that contains the preference objects
        /// </summary>
        public Settings? Settings { get; set; }
        /// <summary>
        /// Convenience class to import/export settings 
        /// </summary>
        private class FdcSettings
        {
            public string? Version { get; set; }
            public Settings? Settings { get; set; }
        }
    }

}
