(function () {
    /*START CONTEXT MENU CLASS*/
    class ContextMenu {
        constructor({ target = null, menuItems = [], mode = "dark" }) {
            this.target = target;
            this.menuItems = menuItems;
            this.mode = mode;
            this.targetNode = this.getTargetNode();
            this.menuItemsNode = this.getMenuItemsNode();
            this.isOpened = false;
        }

        getTargetNode() {
            const nodes = document.querySelectorAll(this.target);

            if (nodes && nodes.length !== 0) {
                return nodes;
            } else {
                console.error(`getTargetNode :: "${this.target}" target not found`);
                return [];
            }
        }

        getMenuItemsNode() {
            const nodes = [];

            if (!this.menuItems) {
                console.error("getMenuItemsNode :: Please enter menu items");
                return [];
            }

            this.menuItems.forEach((data, index) => {
                const item = this.createItemMarkup(data);
                item.firstChild.setAttribute(
                    "style",
                    `animation-delay: ${index * 0.08}s`
                );
                nodes.push(item);
            });

            return nodes;
        }

        createItemMarkup(data) {
            const button = document.createElement("BUTTON");
            const item = document.createElement("LI");

            button.innerHTML = data.content;
            button.classList.add("contextMenu-button");
            item.classList.add("contextMenu-item");

            if (data.divider) item.setAttribute("data-divider", data.divider);
            item.appendChild(button);
            if (data.rightContent) {
                const rightContent = document.createElement("BUTTON");
                rightContent.innerHTML = data.rightContent;
                rightContent.classList.add("contextMenu-label");
                item.appendChild(rightContent);
            }

            if (data.events && data.events.length !== 0) {
                Object.entries(data.events).forEach((event) => {
                    const [key, value] = event;
                    item.addEventListener(key, value);
                });
            }

            return item;
        }

        renderMenu() {
            const menuContainer = document.createElement("UL");

            menuContainer.classList.add("contextMenu");
            menuContainer.setAttribute("data-theme", this.mode);

            this.menuItemsNode.forEach((item) => menuContainer.appendChild(item));

            return menuContainer;
        }

        closeMenu(menu) {
            if (this.isOpened) {
                this.isOpened = false;
                menu.remove();
            }
        }

        init() {
            const contextMenu = this.renderMenu();
            document.addEventListener("click", () => this.closeMenu(contextMenu));
            window.addEventListener("blur", () => this.closeMenu(contextMenu));
            document.addEventListener("contextmenu", (e) => {
                this.targetNode.forEach((target) => {
                    if (!e.target.contains(target)) {
                        contextMenu.remove();
                    }
                });
            });

            this.targetNode.forEach((target) => {
                target.addEventListener("contextmenu", (e) => {
                    e.preventDefault();
                    this.isOpened = true;

                    const { clientX, clientY } = e;
                    document.body.appendChild(contextMenu);

                    const positionY =
                        clientY + contextMenu.scrollHeight >= window.innerHeight
                            ? window.innerHeight - contextMenu.scrollHeight - 20
                            : clientY;
                    const positionX =
                        clientX + contextMenu.scrollWidth >= window.innerWidth
                            ? window.innerWidth - contextMenu.scrollWidth - 20
                            : clientX;

                    contextMenu.setAttribute(
                        "style",
                        `--width: ${contextMenu.scrollWidth}px;
          --height: ${contextMenu.scrollHeight}px;
          --top: ${positionY}px;
          --left: ${positionX}px;`
                    );
                });
            });
        }
    }
    /*END CONTEXT MENU CLASS*/

    /* Start example menu*/
    //Prep menu icons
    const copyIcon = `<img class="context-icon" src="/images/icons/copy.svg" />`;
    const cutIcon = `<svg viewBox="0 0 24 24" width="13" height="13" stroke="currentColor" stroke-width="2.5" style="margin-right: 7px" fill="none" stroke-linecap="round" stroke-linejoin="round" class="css-i6dzq1"><circle cx="6" cy="6" r="3"></circle><circle cx="6" cy="18" r="3"></circle><line x1="20" y1="4" x2="8.12" y2="15.88"></line><line x1="14.47" y1="14.48" x2="20" y2="20"></line><line x1="8.12" y1="8.12" x2="12" y2="12"></line></svg>`;
    const pasteIcon = `<img class="context-icon" src="/images/icons/paste.svg" />`;
    const downloadIcon = `<img class="context-icon" src="/images/icons/download.svg" />`;
    const deleteIcon = `<svg viewBox="0 0 24 24" width="13" height="13" stroke="currentColor" stroke-width="2.5" fill="none" style="margin-right: 7px" stroke-linecap="round" stroke-linejoin="round" class="css-i6dzq1"><polyline points="3 6 5 6 21 6"></polyline><path d="M19 6v14a2 2 0 0 1-2 2H7a2 2 0 0 1-2-2V6m3 0V4a2 2 0 0 1 2-2h4a2 2 0 0 1 2 2v2"></path></svg>`;
    const emptyIcon = `<svg viewBox="0 0 24 24" width="13" height="13"></svg>`;

    //gen menu items
    const menuItems = [
        {
            content: `${copyIcon}Copy asdf asdf asdfs adfasdfasdf`,
            rightContent: `Ctrl+C`,
            events: {
                click: (e) => console.log(e, "Copy Button Click")
                // mouseover: () => console.log("Copy Button Mouseover")
            },
        },
        { content: `${pasteIcon}Paste` },
        { content: `${cutIcon}Cut` },
        { content: `${downloadIcon}Download` },
        {
            content: `${emptyIcon}Delete`,
            divider: "top" // top, bottom, top-bottom
        }
    ];

    //Attach a menu to a class
    //const testContext = new ContextMenu({
    //    target: ".target-testContext",
    //    menuItems
    //});
    //gen the menu
    /*testContext.init();*/
    /* End example menu */ 
})();