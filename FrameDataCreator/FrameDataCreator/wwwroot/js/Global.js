﻿//-----Meant for Global js functions that need to be declared before blazor.js

function updateImagePath(domId) {
    var input = document.getElementById(domId +'imageFile');
    var filepath = input.files[0].path;
    input.nodeValue = filepath;
    return filepath;
};
function updateAnimationPath(domId) {
    var input = document.getElementById(domId + 'animationFolder');
    var filepath = input.files[0].path;
    filepath = filepath.substring(0, filepath.lastIndexOf('\\') + 1);
    input.nodeValue = filepath;
    input.value = null;
    return filepath;
}; 

function saveAsFile(filename, bytesBase64) {
    var link = document.createElement('a');
    link.download = filename;
    link.href = "data:application/octet-stream;base64," + bytesBase64;
    document.body.appendChild(link); 
    link.click();
    document.body.removeChild(link);
}

function getFilePath(domId) {
    var input = document.getElementById(domId);
    var filepath = input.files[0].path;
    input.files[0].path = null;
    input.nodeValue = null;
    input.files[0] = null;
    input.value = null;
    document.getElementById("menuFile").click();
    return filepath;
}

function scrollImage(valueX, valueY) {
    let columnHeight = $('.image-overflow:visible').parent().parent().height();
    let columnWidth = $('.image-overflow:visible').parent().parent().parent().width();

    $('.image-overflow').scrollLeft(valueX + 960 - columnWidth/2);
    $('.image-overflow').scrollTop(valueY + 540 - columnHeight/2);
}

function setDragAndResize(domId, objRef) {
    $("#" + domId)
        .resizable(
        {
                /*helper: "ui-resizable-helper",*/ //<-- There's a bug with the helper sizes when resizing (N or W only) it offsets the result by some pixels
                grid: [1, 1],
                autoHide: true,
                handles: "n, e, s, w, ne, se, sw, nw",
                classes: {
                    "ui-resizable-se": "",
                },
                start: function (event, ui) {
                    ui.element.click();
                },
                stop: function(event, ui) {
                    updateResizeCoordinate(ui, objRef);
                },
                resize: function (event, ui) {
                    $('div.ui-resizable-handle').hide();
                },
            })
        .draggable(
            {
                start: function (event, ui) {
                    ui.helper.click();         
                },
                cursor: "move",
                grid: [1, 1],
                handle: "span",
                stop: function (event, ui) {
                    updateDragCoordinate(ui, ui.helper, objRef);
                },
            });

}
function updateDragCoordinate(ui,domId, objRef) {
    objRef.invokeMethodAsync('OnUpdateCoordinate',
        domId.attr('id'),
        domId.width(),
        domId.height(),
        ui.position.left - 960,
        ui.position.top - 540);
}
function updateResizeCoordinate(ui, objRef) {
    objRef.invokeMethodAsync('OnUpdateCoordinate',
        ui.element.attr('id'),
        ui.size.width,
        ui.size.height,
        ui.originalElement[0].offsetLeft -960,
        ui.originalElement[0].offsetTop -540
    );
}

//Used for NewProjectDialog
window.newProjectDialogDotNet = null;
window.assignDotNetHelper = (dotNetHelper) => {
    window.newProjectDialogDotNet = dotNetHelper;
}

//Used for PreferenceDialog
window.preferenceDotNet = null;
window.preferenceDotNetHelper = (dotNetHelper) => {
    window.preferenceDotNet = dotNetHelper;
}