﻿using FrameDataCreator.Constants;
using FrameDataCreator.Models;
using FrameDataCreator.Models.Attributes;
using FrameDataCreator.Models.Common;
using System.Collections.Generic;
using System.Reflection;

namespace FrameDataCreator.Utilities
{
    /// <summary>
    /// Utility class to handle json convertion from the FDC project json to a game ready json
    /// </summary>
    public static class ExportForGame
    {
        /// <summary>
        /// Gets a dictionnary of key/object from a layer class object's properties
        /// </summary>
        /// <param name="layer">Layer to get properties from</param>
        /// <param name="layerToProp">Is layer to property setting on</param>
        /// <param name="hitboxToList">Is hit box to list setting on</param>
        /// <param name="attributeToProp">Is attribute to property setting on</param>
        /// <returns>Dictionnary map of a layer's properties</returns>
        public static Dictionary<string, object> GetLayerProperties(Layer layer, bool layerToProp, bool hitboxToList, bool attributeToProp)
        {
            Dictionary<string, object> dicProperties = new();
            if (layer.Export)
            {
                foreach (var layerProperty in layer.GetType().GetProperties())
                {
                    AddProperty(ref dicProperties, ref layer, layerProperty, layerToProp, hitboxToList, attributeToProp);
                }
            }
            return dicProperties;
        }
        /// <summary>
        /// Add a property of layer to the dictionnary of the layer's properties, converting them according to settings as needed
        /// </summary>
        /// <param name="dicProperties">Dictionnary reference</param>
        /// <param name="layer">Layer to get properties from</param>
        /// <param name="layerProperty">The current property being handled</param>
        /// <param name="layerToProp">Is layer to property setting on</param>
        /// <param name="hitboxToList">Is hit box to list setting on</param>
        /// <param name="attributeToProp">Is attribute to property setting on</param>
        private static void AddProperty(ref Dictionary<string, object> dicProperties, ref Layer layer, PropertyInfo? layerProperty, bool layerToProp, bool hitboxToList, bool attributeToProp)
        {
            //don't add FDC only properties
            if (IsNotFDCProperty(layerProperty.Name))
            {
                switch (layerProperty.Name)
                {
                    case "Attributes":
                        if (attributeToProp)
                        {
                            ConvertAttributesProperty(layer, hitboxToList, ref dicProperties);
                        }
                        else
                        {
                            dicProperties.Add(layerProperty.Name, layerProperty.GetValue(layer));
                        }
                        break;
                    case "Children":
                        if (layerToProp)
                        {
                            ConvertChildrenProperty(layer, ref dicProperties, layerToProp, hitboxToList, attributeToProp);
                        }
                        else
                        {
                            dicProperties.Add(layerProperty.Name, layerProperty.GetValue(layer));
                        }
                        break;
                    default:
                        if (!IsContainerOnly(layer))
                        {
                            dicProperties.Add(layerProperty.Name, layerProperty.GetValue(layer));
                        }
                        break;
                }
            }
        }
        /// <summary>
        /// Converts a children property and recursively call GetLayerProperties on children with children
        /// </summary>
        /// <param name="layer">The layer</param>
        /// <param name="dicProperties">The layer's dictionnary reference</param>
        /// <param name="layerToProp">Is layer to property setting on</param>
        /// <param name="hitboxToList">Is hit box to list setting on</param>
        /// <param name="attributeToProp">Is attribute to property setting on</param>
        private static void ConvertChildrenProperty(Layer layer, ref Dictionary<string, object> dicProperties, bool layerToProp, bool hitboxToList, bool attributeToProp)
        {
            var childAsList = new List<Dictionary<string, object>>();
            foreach (var layerChild in layer.Children)
            {
                if (layerChild.Export)
                {
                    var childDicProperties = GetLayerProperties(layerChild, layerToProp, hitboxToList, attributeToProp);
                    //Dead layer is only a container of a list
                    if (IsContainerOnly(layer))
                    {
                        childAsList.Add(childDicProperties);
                    }
                    else
                    {
                        dicProperties.Add(layerChild.Name, childDicProperties);
                    }
                }
            }
            if (childAsList.Count > 0)
            {
                dicProperties.Add(layer.Name, childAsList);
            }
        }
        /// <summary>
        /// Converts attributes property to add to the parent's properties
        /// Hit boxes are handled a little bit different if the setting is on as it will create a list for each valuetype of hitboxes
        /// </summary>
        /// <param name="layer">The layer</param>
        /// <param name="hitboxToList">Is hit box to list setting on</param>
        /// <param name="dicProperties">The parent layer's dictionnary reference</param>
        private static void ConvertAttributesProperty(Layer layer, bool hitboxToList, ref Dictionary<string, object> dicProperties)
        {
            List<List<Dictionary<string, object>>> listOfListsOfHitboxProperties = new();
            foreach (var attribute in layer.Attributes)
            {
                bool isHitBox = attribute.PropertyType == SD.HitBox;
                Dictionary<string, object> dicAttributeProperties = new();
                int hitBoxType = isHitBox ? Convert.ToInt32(attribute.Value) : 0;
                #region old stuff
                //foreach (var attributeProperty in attribute.GetType().GetProperties())
                //{
                //    //don't add FDC only properties
                //    if (IsNotFDCAttribute(attributeProperty.Name) ||
                //        (attributeProperty.Name == "Name" && isHitBox))
                //    {
                //        if (hitboxToList && isHitBox)
                //        {
                //            dicAttributeProperties.Add(attributeProperty.Name, attributeProperty.GetValue(attribute));
                //        }
                //        else
                //        {
                //            if (ValidProperty(attributeProperty, attribute))
                //            {
                //                try
                //                {
                //                    dicProperties.Add(attributeProperty.Name, attributeProperty.GetValue(attribute));
                //                }
                //                catch (Exception e)
                //                {
                //                    Console.WriteLine(attributeProperty.Name + "\n" + attribute.Name);
                //                }
                //            }
                //        }
                //    }
                //}
                #endregion
                if (hitboxToList && isHitBox)
                {
                    foreach (var attributeProperty in attribute.GetType().GetProperties())
                    {
                        //don't add FDC only properties
                        if (IsNotFDCAttribute(attributeProperty.Name) ||
                            (attributeProperty.Name == "Name" && isHitBox))
                        {
                            dicAttributeProperties.Add(attributeProperty.Name, attributeProperty.GetValue(attribute));

                        }
                    }
                    if (listOfListsOfHitboxProperties.Count < hitBoxType + 1)
                    {
                        for (int i = listOfListsOfHitboxProperties.Count; i < hitBoxType + 1; i++)
                        {
                            listOfListsOfHitboxProperties.Add(new List<Dictionary<string, object>>());
                        }
                    }
                    listOfListsOfHitboxProperties[hitBoxType].Add(dicAttributeProperties);
                }
                else
                {
                    Dictionary<string, object> attributeValues = GetAttributeAsMap(attribute);
                    foreach (var item in attributeValues)
                    {
                        dicProperties.Add(item.Key, item.Value);
                    }
                }
            }
            if (listOfListsOfHitboxProperties.Count > 0)
            {
                foreach (var hitBoxList in listOfListsOfHitboxProperties)
                {
                    if (hitBoxList.Count > 0)
                    {
                        dicProperties.Add(hitBoxList.First()["Name"].ToString(), hitBoxList);
                    }
                }
            }
        }
        /// <summary>
        /// Gets the attribute as a map of key value
        /// </summary>
        /// <param name="attribute">The attribute to get values from</param>
        /// <returns>Dictionnary mapping of key/values of the attribute's property</returns>
        private static Dictionary<string, object> GetAttributeAsMap(IAttribute attribute)
        {
            Dictionary<string, object> attributeValues = new();
            switch (attribute.PropertyType)
            {
                case SD.Animation:
                    break;
                case SD.BoolCheck:
                case SD.Integer:
                case SD.Real:
                case SD.Text:
                case SD.RadioChoice:
                case SD.ListChoice:
                case SD.ListFromGroup:
                    attributeValues.Add(attribute.Name, attribute.Value);
                    break;
                case SD.Origin:
                    Dictionary<string, object> originMap = new();
                    originMap.Add("X", ((attribute as Origin).Value as Position).X);
                    originMap.Add("Y", ((attribute as Origin).Value as Position).Y);
                    attributeValues.Add(attribute.Name, originMap);
                    break;
                case SD.HitBox:
                    Dictionary<string, object> hitboxMap = new();
                    hitboxMap.Add("Value", (attribute as HitBox).Value);
                    hitboxMap.Add("Height", (attribute as HitBox).Height);
                    hitboxMap.Add("Width", (attribute as HitBox).Width);
                    hitboxMap.Add("Position", (attribute as HitBox).Position);
                    hitboxMap.Add("Color", (attribute as HitBox).Color);
                    attributeValues.Add(attribute.Name, hitboxMap);
                    break;
                case SD.FrameImage:
                    attributeValues.Add("Index", (attribute as FrameImage).Index);
                    attributeValues.Add("Duration", (attribute as FrameImage).Duration);
                    break;
            }
            return attributeValues;
        }

        /// <summary>
        /// Checks if the the property is a valid property to handle
        /// This is used specifically for some attributes which we don't want to see the gameId property
        /// </summary>
        /// <param name="attributeProperty">The name of the property</param>
        /// <param name="attribute">The attribute containing the property</param>
        /// <returns>True if valid</returns>
        private static bool ValidProperty(PropertyInfo attributeProperty, IAttribute attribute)
        {
            return (attributeProperty.Name == "GameId" &&
                !String.IsNullOrEmpty(attributeProperty.GetValue(attribute).ToString())) ||
                attributeProperty.Name != "GameId" && attribute.PropertyType != SD.Animation;
        }
        /// <summary>
        /// Checks if the layer is a container only
        /// </summary>
        /// <param name="layer">The layer to check</param>
        /// <returns>True if the layer is an empty container</returns>
        private static bool IsContainerOnly(Layer layer)
        {
            return (layer.Children.Count > 0 && layer.Attributes.Count == 0);
        }
        /// <summary>
        /// Validator if the property of the layer should be visible or if it is only used internally by FDC
        /// </summary>
        /// <param name="propertyName">The property name</param>
        /// <returns>True if the property is not an internal FDC property</returns>
        private static bool IsNotFDCProperty(string propertyName)
        {
            if (propertyName != "Parent" &&
                propertyName != "ParentId" &&
                propertyName != "OriginalTemplateId" &&
                propertyName != "Id" &&
                propertyName != "Export"
            )
            {
                return true;
            }
            return false;
        }
        /// <summary>
        /// Validator if the property of the attribute should be visible or if it is only used internally by FDC
        /// </summary>
        /// <param name="propertyName">The property name</param>
        /// <returns>True if the property is not an internal FDC property</returns>
        private static bool IsNotFDCAttribute(string propertyName)
        {
            if (propertyName != "Name" &&
            propertyName != "Default" &&
            propertyName != "PropertyType" &&
            propertyName != "Id" &&
            propertyName != "ImagePath" &&
            propertyName != "ImageListPath" &&
            propertyName != "ImageTemplateToCreate" &&
            propertyName != "ImagesLayersUID" &&
            propertyName != "Min" &&
            propertyName != "Max" &&
            propertyName != "Increment"
            )
            {
                return true;
            }
            return false;
        }
    }
}
