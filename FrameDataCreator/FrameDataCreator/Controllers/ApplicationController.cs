﻿using FrameDataCreator.Constants;
using FrameDataCreator.Data;
using FrameDataCreator.Models;
using FrameDataCreator.Models.Attributes;
using FrameDataCreator.Models.Common;
using FrameDataCreator.Utilities;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

namespace FrameDataCreator.Controllers
{
    /// <summary>
    /// Application Controller to communicate with the App State and return data to views
    /// </summary>
    public class ApplicationController : Controller
    {
        protected readonly FDCState _fdc;
        private Layer _copyingLayer = null;
        private List<IAttribute> _copyingBoxes = new();
        /// <summary>
        /// Basic Constructor
        /// </summary>
        /// <param name="fDCState">DI application state</param>
        public ApplicationController(FDCState fDCState)
        {
            _fdc = fDCState;
        }
        /// <summary>
        /// Get the project reference
        /// </summary>
        /// <returns>The current project reference</returns>
        public Project GetProject()
        {
            Project = _fdc.Project;
            return Project;
        }
        public void CopyLayer(Layer layer)
        {
            _copyingLayer = layer.GetCloned();
        }
        public void PasteLayer()
        {
            if (LayerContext != null)
            {
                LayerContext.AddChild(_copyingLayer);
            }
            else
            {
                Project.Layers.Add(_copyingLayer);
            }
        }
        /// <summary>
        /// Gets a list of layers from a specific gameId of all layers with that Id
        /// </summary>
        /// <param name="gameId">The gameId to look for</param>
        /// <returns>A list of pairs of Id and Name</returns>
        public List<KeyValuePair<string, string>> GetListFromLayer(string gameId)
        {
            var result = new List<KeyValuePair<string, string>>();
            foreach (var layer in Project.Layers)
            {
                var parent = FindLayerRecursiveGameId(layer, gameId);
                if (FindLayerRecursiveGameId(layer, gameId) != null)
                {
                    foreach (var child in parent.Children)
                    {
                        result.Add(new KeyValuePair<string, string>(child.GameId, child.Name));
                    }
                }
            }
            return result;
        }
        /// <summary>
        /// Gets a list of project prefabs
        /// </summary>
        /// <returns>Reference to the prefab list</returns>
        public List<string> GetPrefabs()
        {
            _fdc.LoadPrefabs();
            return _fdc.Prefabs;
        }
        /// <summary>
        /// Gets the current layer context and verifies if it is missing attributes from its template
        /// </summary>
        /// <returns>The current layer context</returns>
        public Layer? GetLayerContext()
        {
            //verify if layer is missing attributes from template
            if (LayerContext != null)
            {
                var template = GetTemplates().FirstOrDefault(t => t.Id == LayerContext.OriginalTemplateId);
                if (template != null)
                {
                    if (template.Attributes.Count != LayerContext.Attributes.Count)
                    {
                        foreach (var attribute in template.Attributes)
                        {
                            if (!LayerContext.Attributes.Any(a => a.Id == attribute.Id))
                            {
                                LayerContext.Attributes.Add(attribute.Clone());
                            }
                        }
                    }
                }
            }
            return LayerContext;
        }
        /// <summary>
        /// Sets the current layer context and verify if it needs to assign an image context and animation context
        /// </summary>
        /// <param name="layerId">The current layer Id set</param>
        public void SetLayerContext(string layerId)
        {
            LayerContext = null;
            foreach (var layer in Project.Layers)
            {
                var result = FindLayerRecursiveStringId(layer, layerId);
                if (result != null)
                {
                    LayerContext = result;

                    var imageContext = LayerContext.FindAttribute<FrameImage>(SD.FrameImage);
                    if (imageContext != null)
                    {
                        ImageContext = imageContext;
                        AnimContext = LayerContext?.Parent?.Parent?.FindAttribute<Animation>(SD.Animation);
                    }
                    else ImageContext = null;

                    var animContext = LayerContext?.FindAttribute<Animation>(SD.Animation);
                    if (animContext != null) { AnimContext = animContext; } //else AnimContext = null;
                }
            }
        }
        /// <summary>
        /// Gets the project's layers as transformed Json (to export for game usage)
        /// </summary>
        /// <returns>Json string of the project's layers</returns>
        /// <see cref="ExportForGame"/>
        public string GetLayersJson()
        {
            try
            {
                bool layerToProp = Convert.ToBoolean(_fdc.Settings.Preferences.First(p => p.Name == "Layers Export").Value);
                bool attributeToProp = Convert.ToBoolean(_fdc.Settings.Preferences.First(p => p.Name == "Attributes Export").Value);
                bool hitboxToList = Convert.ToBoolean(_fdc.Settings.Preferences.First(p => p.Name == "Hitbox Export").Value);
                List<Dictionary<string, object>> project = new();
                foreach (var layer in Project.Layers)
                {
                    var dicProperties = ExportForGame.GetLayerProperties(layer, layerToProp, hitboxToList, attributeToProp);
                    project.Add(dicProperties);
                }

                return JsonConvert.SerializeObject(project, Formatting.Indented);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message + "\n" + e.StackTrace);
                return String.Empty;
            }
        }
        /// <summary>
        /// Gets a list of templates which contain a certain attribute type
        /// </summary>
        /// <param name="attributeType">The attribute type to look for or 0 for any</param>
        /// <returns>A list of templates</returns>
        public List<Template> GetTemplates(int attributeType = 0)
        {
            var result = Project.Templates;
            if (attributeType > 0)
            {
                result = Project.Templates.Where(t => t.Attributes.Any(a => a.PropertyType == attributeType)).ToList();
            }
            return result;
        }
        /// <summary>
        /// Sets the current Image context
        /// </summary>
        /// <param name="imageFrame">The frame reference</param>
        public void SetImageContext(FrameImage imageFrame)
        {
            ImageContext = imageFrame;
        }
        /// <summary>
        /// Returns the current Image context
        /// </summary>
        /// <returns>The frame reference</returns>
        public FrameImage? GetImageContext()
        {
            return ImageContext;
        }
        /// <summary>
        /// Sets the current animation context
        /// </summary>
        /// <param name="animation">The animation reference</param>
        public void SetAnimationContext(Animation animation)
        {
            AnimContext = animation;
        }
        /// <summary>
        /// Gets the current animation context
        /// </summary>
        /// <returns></returns>
        public Animation? GetAnimationContext()
        {
            return AnimContext;
        }
        /// <summary>
        /// Sets the current template context
        /// </summary>
        /// <param name="templateId">The Id of the template to select</param>
        public void SetTemplateContext(string templateId)
        {
            TemplateContext = Project?.Templates?.FirstOrDefault(x => x.Id == templateId);
        }
        /// <summary>
        /// Gets the current selected template
        /// </summary>
        /// <returns>Current template selected</returns>
        public Template? GetTemplateContext()
        {
            return TemplateContext;
        }
        /// <summary>
        /// Renames layer attributes that are based on this template after an attribute name change
        /// </summary>
        /// <param name="template">The original template </param>
        public void RenameLayersAttributeFromTemplate(Template template)
        {
            foreach (var layer in Project.Layers)
            {
                RenameAttributeRecursive(layer, template);
            }
        }


        /// <summary>
        /// Delete a template attribute
        /// </summary>
        /// <param name="property">The attribute to delete</param>
        public void DeleteTemplateProperty(IAttribute property)
        {
            TemplateContext.Attributes.Remove(property);
            foreach (var layer in Project.Layers)
            {
                RemovePropertyRecursive(layer, property.Id, TemplateContext.Id);
            }
        }

        /// <summary>
        /// Verifies if the current layer has an empty container or creates it
        /// </summary>
        /// <returns>The layer with the frames container</returns>
        public Layer VerifyFrameContainer()
        {
            var autoChild = Convert.ToBoolean(GetPreferences().First(n => n.Name == "Animation AutoChild").Value);
            if (autoChild)
            {
                //check if there is already an empty container called frames
                var container = LayerContext.Children.FirstOrDefault(c => c.Name == "Frames");
                if (container == null)
                {
                    //if no container, verify if there is a template called Frames and create it, then create layer
                    var templateContainer = GetTemplates().FirstOrDefault(c => c.Name == "Frames");
                    if (templateContainer == null)
                    {
                        CreateTemplate("Frames");
                        templateContainer = GetTemplates().First(c => c.Name == "Frames");
                    }
                    CreateNewLayer(templateContainer);
                    container = LayerContext.Children.First(c => c.Name == "Frames");
                }
                return container;
            }
            else
            {
                return LayerContext;
            }
        }
        /// <summary>
        /// Creates Frame Images from a folder from an animation attribute
        /// </summary>
        /// <param name="animation">The animation attribute to get data from</param>
        public void CreateImagesFromFolder(Animation animation)
        {
            try
            {
                if (LayerContext != null)
                {
                    Layer imageContainer = VerifyFrameContainer();
                    imageContainer.Children.RemoveAll(child => child.OriginalTemplateId == animation.ImageTemplateToCreate);
                    animation.ImagesLayersUID = new List<string>();
                    // Process the list of files found in the directory.
                    List<string> fileEntries = Directory.GetFiles((String)animation.Value)?.Where(s => s.EndsWith(".png") || s.EndsWith(".jpg"))?.ToList();
                    if (fileEntries == null)
                    {
                        throw new Exception($"File entries null in CreateImagesFromFolder for folder path: {animation.Value}");
                    }
                    else
                    {
                        fileEntries.Sort();
                        animation.ImageListPath = new List<string>(fileEntries);
                        var templateToClone = Project.Templates.First(t => t.Id == animation.ImageTemplateToCreate);
                        SetLayerContext(imageContainer.Id);
                        for (int i = 0; i < fileEntries.Count; i++)
                        {
                            CreateNewLayer(templateToClone);
                            var layerImage = imageContainer.Children.Last();
                            var image = layerImage.FindAttribute<FrameImage>(SD.FrameImage);
                            image.ImagePath = fileEntries[i];
                            image.Name = $"Frame";// TODO put this as an option => image.Name = $"Frame_00{i}";
                            image.Index = i;
                            animation.ImagesLayersUID.Add(layerImage.Id);
                        }
                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message + "\n" + e.StackTrace);
                //return error message
            }
            animation.Value = String.Empty;
        }
        /// <summary>
        /// Creates a new hit box using a template
        /// </summary>
        /// <param name="template">Template of hitbox to clone</param>
        /// <returns>The new HitBox</returns>
        public HitBox CreateHitBox(Template template, Position position = null)
        {
            var hitboxClone = template.FindAttribute<HitBox>(SD.HitBox)?.Clone();
            if (position != null)
            {
                (hitboxClone as HitBox).Position = position;
            }
            LayerContext.Attributes.Add(hitboxClone);
            return hitboxClone as HitBox;
        }
        public HitBox CopyHitBox(HitBox hitBox)
        {
            var hitboxClone = hitBox.Clone();
            LayerContext.Attributes.Add(hitboxClone);
            return hitboxClone as HitBox;
        }
        public void CopyCurrentBoxes()
        {
            _copyingBoxes.Clear();
            foreach (var attr in LayerContext.Attributes)
            {
                if (attr.PropertyType == SD.HitBox)
                {
                    _copyingBoxes.Add(attr.Clone());
                }
            }
            
        }
        public void PasteSavedBoxes()
        {
            //Remove current hitboxes from current frame
            LayerContext.Attributes.RemoveAll(a => a.PropertyType == SD.HitBox);
            foreach (var hitbox in _copyingBoxes)
            {
                LayerContext.Attributes.Add(hitbox.Clone());
            }
        }
        /// <summary>
        /// Clone previous frame's hit boxes to the current frame
        /// </summary>
        /// <returns>true if cloned correctly</returns>
        public bool ClonePreviousFrameHitBoxes()
        {
            var parent = LayerContext.Parent;
            if (parent != null)
            {
                int currentFrameIndex = parent.Children.FindIndex(x => x.Id == LayerContext.Id);
                if (currentFrameIndex > 0)
                {
                    //find previous frame
                    var previousFrameIndex = currentFrameIndex - 1;
                    //Remove current hitboxes from current frame
                    LayerContext.Attributes.RemoveAll(a => a.PropertyType == SD.HitBox);
                    foreach (var previousAttribute in parent.Children[previousFrameIndex].Attributes)
                    {
                        if (previousAttribute.PropertyType == SD.HitBox)
                        {
                            //clone attributes and add to current layercontext
                            LayerContext.Attributes.Add(previousAttribute.Clone());
                        }
                    }
                    return true;
                }
            }
            return false;
        }
        /// <summary>
        /// Creates a new layer using a template
        /// </summary>
        /// <param name="template">The template to generate from</param>
        public void CreateNewLayer(Template template)
        {
            var clone = template.Clone();
            if (LayerContext != null)
            {
                LayerContext.AddChild(new Layer(template.Id) { Attributes = clone.Attributes, Name = clone.Name, GameId = clone.GameId });
            }
            else
            {
                Project.Layers.Add(new Layer(template.Id) { Attributes = clone.Attributes, Name = clone.Name, GameId = clone.GameId });
            }
        }
        /// <summary>
        /// Creates a new template
        /// </summary>
        /// <param name="name">The name of the template</param>
        public void CreateTemplate(string name = "New Template")
        {
            Project?.Templates.Add(new Template(name));
        }
        /// <summary>
        /// Delete current layer selected
        /// </summary>
        public void DeleteLayerContext()
        {
            if (LayerContext != null)
            {
                foreach (var layer in Project.Layers)
                {
                    if (layer.Id == LayerContext.Id) { Project.Layers.Remove(layer); break; }
                    var result = FindLayerRecursiveStringId(layer, LayerContext.Id);
                    result?.Parent.Children.Remove(result);
                }
            }
            LayerContext = null;
        }
        /// <summary>
        /// Deletes a prefab project
        /// </summary>
        /// <param name="prefabName">The name of the prefab to delete</param>
        public void DeletePrefab(string prefabName)
        {
            _fdc.DeletePrefab(prefabName);
        }
        /// <summary>
        /// Renames a prefab project
        /// </summary>
        /// <param name="newName">New name to be given</param>
        /// <param name="oldName">The old name of the prefab</param>
        public void RenamePrefab(string newName, string oldName)
        {
            _fdc.RenamePrefab(newName, oldName);
        }
        /// <summary>
        /// Deletes a template from the list of templates
        /// </summary>
        /// <param name="templateId">Template Id of the template to delete</param>
        public void DeleteTemplate(string templateId)
        {
            Project?.Templates.Remove(
                Project.Templates.First(t => t.Id == templateId)
            );
        }
        /// <summary>
        /// Clone a template to add to the list of templates
        /// </summary>
        /// <param name="templateId">Template Id of the template to clone</param>
        public void CloneTemplate(string templateId)
        {
            Project?.Templates.Add(
                Project.Templates.First(t => t.Id == templateId).Clone()
            );
        }
        /// <summary>
        /// Creates a new project and resets context and project references
        /// </summary>
        /// <param name="name">Name of the new project</param>
        /// <param name="prefabName">Name of the prefab used</param>
        /// <param name="blank">True if blank project</param>
        public void NewProject(string name, string prefabName = "", bool blank = true)
        {
            _fdc.NewProject(name, prefabName, blank);
            _ = GetProject();
            LayerContext = null;
            AnimContext = null;
            ImageContext = null;
            TemplateContext = null;
        }
        /// <summary>
        /// Gets the project as json
        /// </summary>
        /// <returns>Json string of the project</returns>
        public string GetProjectJson()
        {
            return JsonConvert.SerializeObject(Project, Formatting.Indented, new JsonSerializerSettings
            {
                TypeNameHandling = TypeNameHandling.All
            });
        }
        /// <summary>
        /// Gets the list of templates as json
        /// </summary>
        /// <returns>Json string of the project's templates list</returns>
        public string GetTemplateJson()
        {
            return JsonConvert.SerializeObject(Project?.Templates, Formatting.Indented, new JsonSerializerSettings
            {
                TypeNameHandling = TypeNameHandling.All
            });
        }
        /// <summary>
        /// Gets the current application version
        /// </summary>
        /// <returns>Version as string</returns>
        public string GetVersion()
        {
            return _fdc.Version ?? "0.0.0";
        }
        /// <summary>
        /// Gets the list of preferences from the app state
        /// </summary>
        /// <returns>The reference to the list of preferences</returns>
        public List<Preference> GetPreferences()
        {
            return _fdc.Settings.Preferences;
        }
        /// <summary>
        /// Save current preference settings
        /// </summary>
        /// <returns>True if the save succeeded</returns>
        public bool SavePreferences()
        {
            return _fdc.SaveSettings();
        }
        /// <summary>
        /// Save current project to file
        /// </summary>
        /// <returns>True if the save succeeded</returns>
        public bool SaveProject()
        {
            return _fdc.SaveProject();
        }
        /// <summary>
        /// Save current project as a Prefab
        /// </summary>
        /// <returns>True if the save succeeded</returns>
        public bool SaveProjectAsPrefab()
        {
            return _fdc.SaveProjectAsPrefab();
        }
        /// <summary>
        /// Opens a saved project and send it to the app state
        /// </summary>
        /// <param name="path">Path to the saved project file</param>
        public void OpenProject(string path)
        {
            if (System.IO.File.Exists(path))
            {
                var projectOpen = JsonConvert.DeserializeObject<Project>(System.IO.File.ReadAllText(path), new JsonSerializerSettings
                {
                    TypeNameHandling = TypeNameHandling.Auto
                });
                if (projectOpen != null)
                {
                    Project = _fdc.OpenProject(projectOpen);
                }
            }
        }
        /// <summary>
        /// Imports a list of templates from a file into the current project
        /// </summary>
        /// <param name="path">Path to the Json file</param>
        public void ImportTemplateJson(string path)
        {
            if (System.IO.File.Exists(path))
            {
                List<Template> resultList = JsonConvert.DeserializeObject<List<Template>>(System.IO.File.ReadAllText(path), new JsonSerializerSettings
                {
                    TypeNameHandling = TypeNameHandling.Auto
                });
                if (resultList != null || resultList?.Count > 0)
                {
                    foreach (var template in resultList)
                    {
                        if (!Project.Templates.Any(t => t.Id == template.Id))
                        {
                            Project.Templates.Add(template);
                        }
                    }
                }
            }
        }
        /// <summary>
        /// Recursively renames all attributes of all layers with the original template's Id
        /// </summary>
        /// <param name="layer">Current layer to check</param>
        /// <param name="template">The original template</param>
        private void RenameAttributeRecursive(Layer layer, Template template)
        {
            if (layer.OriginalTemplateId == template.Id)
            {
                foreach (var attribute in layer.Attributes)
                {
                    var templateAttribute = template.Attributes.FirstOrDefault(a => a.Id == attribute.Id);
                    if (templateAttribute != null)
                    {
                        attribute.Name = templateAttribute.Name;
                    }
                }
            }
            foreach (var childLayer in layer.Children)
            {
                RenameAttributeRecursive(childLayer, template);
            }
        }
        /// <summary>
        /// Recursive function to remove an attribute from all layers
        /// </summary>
        /// <param name="layer">Current Layer</param>
        /// <param name="id">Id of the attribute</param>
        /// <param name="templateId">The original template Id of the attribute</param>
        private void RemovePropertyRecursive(Layer layer, string id, string templateId)
        {
            if (layer.OriginalTemplateId == templateId)
            {
                layer.Attributes.RemoveAll(p => p.Id == id);
            }
            foreach (var childLayer in layer.Children)
            {
                RemovePropertyRecursive(childLayer, id, templateId);
            }
        }
        /// <summary>
        /// Recursive function to find a layer with a specified Id
        /// </summary>
        /// <param name="layer">Current layer</param>
        /// <param name="id">Id to search</param>
        /// <returns>Null or layer found</returns>
        private Layer? FindLayerRecursiveStringId(Layer layer, string id)
        {
            if (layer.Id == id) { return layer; }
            foreach (var childLayer in layer.Children)
            {
                var result = FindLayerRecursiveStringId(childLayer, id);
                if (result != null) { return result; }
            }
            return null;
        }
        /// <summary>
        /// Recursive function find a layer with a specified gameId
        /// </summary>
        /// <param name="layer">Layer searching</param>
        /// <param name="gameId">Id to search</param>
        /// <returns>Null or the layer found</returns>
        private Layer? FindLayerRecursiveGameId(Layer layer, string gameId)
        {
            if (layer.GameId == gameId) { return layer; }
            foreach (var childLayer in layer.Children)
            {
                var result = FindLayerRecursiveGameId(childLayer, gameId);
                if (result != null) { return result; }
            }
            return null;
        }
        /// <summary>
        /// Recursive function to find a layer with a specified original templateId
        /// </summary>
        /// <param name="layer">The current layer</param>
        /// <param name="id">The template Id to search for</param>
        /// <returns>Null or the layer found</returns>
        private Layer? FindLayerRecursiveByTemplateId(Layer layer, string id)
        {
            if (layer.Id == id) { return layer; }
            foreach (var childLayer in layer.Children)
            {
                var result = FindLayerRecursiveByTemplateId(childLayer, id);
                if (result != null) { return result; }
            }
            return null;
        }
        /// <summary>
        /// Current project
        /// </summary>
        private Project? Project { get; set; }
        /// <summary>
        /// Current Layer Context (selected layer)
        /// </summary>
        private Layer? LayerContext { get; set; }
        /// <summary>
        /// Current Template Context (selected template)
        /// </summary>
        private Template? TemplateContext { get; set; }
        /// <summary>
        /// Current Animation Context (last selected animation)
        /// </summary>
        private Animation? AnimContext { get; set; }
        /// <summary>
        /// Current Image Context (last selected image)
        /// </summary>
        private FrameImage? ImageContext { get; set; }
    }
}
