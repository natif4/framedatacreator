## Links

| Version    |
| ----------- | 
| [1.0.0](#1.0.0)        | 
| [0.1.0](#0.1.0)        | 

# 1.0.0

- Option to export layers as direct properties
- Option to export attributes as direct properties
- Option to export hitboxes as direct list property grouped by hitbox value
- General improvements of classes
- Animation manipulation (speed -/+, pause, play)
- Animation attribute create frames' layer container automatically
- Enable Project Prefabs deletion and name edit in preferences
- Removeing an attribute from a template will also remove the attribute from any layers based on it
- Adding an attribute to a template will also get added to layers based on it when they are refetched from the appcontroller
- Added onion skinning in Image View and Animation View
- Added docs
- Added static prefabs in appsettings
- Added tests

# 0.1.0

- Able to use the basic functionnality to generate a json containing layers of data.
- Export for game as json
- Create/save/open projects
- Import/Export templates
- Create project prefabs
- Add attributes to templates (int,text, real, choice list, choice radio, etc.)
- Create layers
- Tree view from layers
- Image view to create/manipulate hitboxes and display image from FrameImage
- Animation view
- Properties view to edit layer data attributes
- Preferences menu (options currently don't have any effect, but more will implemented in future versions)
- Able to change area layouts (resize)
